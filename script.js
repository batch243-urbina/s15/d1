// console.log("Hello World!");

// Syntax, Statements and Comments

// Statements in programming are instructions that we tell the computer to perform.
// JS statements usually end with semicolon(;)
// A syntax in programming, it is the set of rules that we describe how statements must be constructed

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code

// Variables
// Used to contain data
// Declaring variables

let myVariable = "Ada Lovelace";
console.log(myVariable);

// Guides in writing variables
// 1. Use let variable name and = "Value"
// 2. Use camelCasing
// 3. Use const for constant values
// 4. Be descriptive

// Declare and initialize
let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.538;
console.log(interest);

// Reassigning a variable
productName = "laptop";
console.log(productName);

let outerVariable = "Hello from the other side";

{
  let innerVariable = "Hello from the block";
  console.log(innerVariable);
}

console.log(outerVariable);

// Multiple declarations

let productCode = "DC017",
  productBrand = "Dell";

console.log(productBrand, productCode);

// Data Types
// String - word, phrase, sentence
//  using '' or ""

let country = "Philippines";
let province = "Metro Manila";

// Concatenate

let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early.";
console.log(message);

let count = "26";
let headcount = 26;
console.log(count);
console.log(headcount);

let grade = 98.7;
console.log(grade);

let planetDistance = 2e10;
console.log(planetDistance);

console.log("John's grade last quarter is " + grade);
console.log(headcount + count);

// Boolean

let isMarried = false;
let inGoodConduct = true;

console.log(isMarried);
console.log(inGoodConduct);

console.log("isMarried: " + isMarried);

// Arrays are special kind of data type that's used to store multiple values
// Arrays can store different data types but is normally used to store similar data types
// Array is special type of object

// Similar data types
// Syntax let/const arrayName = [elementA, elementB ...]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);
console.log(grades[1]);

let details = ["John", 32, true];
console.log(details);

// Objects special kind of data type that is used to mimic real world objects
// Syntax let/const objectName = {propertyA: value, propertyB: value}

let person = {
  fullName: "Juan Dela Cruz",
  age: 35,
  isMarried: false,
  contact: ["09172134567", "8123 4456"],
  address: { houseNumber: "345", city: "Manila" },
};

console.log(person);
console.log(person.age);
console.log(person.contact[1]);
console.log(person.address.city);

// TypeOf operator
console.log(typeof person);
console.log(typeof details);

/*
Constant Objects and Arrays
The keyword const is a little misleading.

It does not define a constant value. It defines a constant reference to a value.

Because of this you can NOT:

Reassign a constant value
Reassign a constant array
Reassign a constant object

But you CAN:

Change the elements of constant array
Change the properties of constant object

*/

const anime = ["ghost fighter", "slam dunk", "highschool dxd"];
console.log(anime);

anime[0] = "pokemon";
console.log(anime);

// Null to express absence of a value in a variable
let spouse = null;
console.log(spouse);

// Undefined represents the state of a variable that has been declared but without value

let fullName;
console.log(fullName);

// Undefined vs Null
// Undefined - declaration only
// Null - value that does not hold any amount
